# php-extended/polyfill-php74-mb-str-split

A polyfill that adds the php74 mb_str_split function added in php7.4 to previous versions of php.

![coverage](https://gitlab.com/php-extended/polyfill-php74-mb-str-split/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/polyfill-php74-mb-str-split/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/polyfill-php74-mb-str-split": "^1"`


## Basic Usage

This library gives the function `mb_str_split(string $string , int $length = 1 , ?string $encoding = null) : string[]` :

```php

$res = \mb_str_split('foobar', 3, 'UTF-8');
// $res is ['foo', 'bar']

```


## License

MIT (See [license file](LICENSE)).
