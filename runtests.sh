#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR="$(dirname "$0")"
CURRDIR="$(realpath "$CURRDIR")"
PAREDIR="$(realpath "$CURRDIR/..")"
cd "$CURRDIR"


# {{{ FUNCTIONS

# Function to copy the file at $1 to $2 if it exists
# @var $1 the file to check if it exists
# @var $2 the path where to copy it if we found it
copy_if_exists() {
	if [ "$RUN_IN_CI" == "1" ]
	then
		if [ -f "$1" ]
		then
			cp "$1" "$2"
		fi
	fi
}

# Function to check the availability of a given binary
# @var $1 the name of the binary to check
check_install() {
	local RES
	
	set +e
	command -v "$1" > /dev/null 2>&1
	RES=$?
	set -e
	if [ $RES != 0 ]
	then
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] Failed to find $1, please install it"
		exit 1
	fi
}

# Function to download from $1 and store the result to $2
# @var $1 the url to download
# @var $2 the local path of the file to save
download() {
	check_install "curl"
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] DOWNLOADING : $1"
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] WRITING TO  : $2"
	curl --location --progress-bar --fail --show-error "$1" --output "$2"
}

# Function to get the full release url for the given library
# @var $1 the vendor name
# @var $2 the library name
get_release_url() {
	local API_URL
	local RELEASE_URL
	
	if [ "$2" == "phpunit" ]
	then
		echo "https://phar.phpunit.de/phpunit-9.phar"
	else
		check_install "curl"
		check_install "jq"
		check_install "sed"
		API_URL="https://api.github.com/repos/$1/$2/releases"
		RELEASE_URL=$(curl --location --progress-bar --fail --show-error "$API_URL" | jq '[.[]|.assets|.[]|.browser_download_url][0]' | sed 's/"//g')
		echo "$RELEASE_URL"
	fi
}

# Ensures that the given phar file is correctly installed
# @var $1 the path of the file where to search it on the file system if available
# @var $2 the path where the file should be executed
# @var $3 the package vendor name in the github url
# @var $4 the package library name in the github url
phar_install() {
	local OLDPSTN
	local RELEASE_URL
	
	check_install "find"
	copy_if_exists "$1" "$2"
	
	set +e
	OLDPSTN=$(find "$2" -mtime +7 -print > /dev/null 2>&1)
	set -e
	if [ ! -f "$2" ] || [ -n "$OLDPSTN" ]
	then
		RELEASE_URL=$(get_release_url "$3" "$4")
		download "$RELEASE_URL" "$2"
	else
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] DO NOT INSTALL $3/$4 : not old enough"
	fi
}

# Tries to install the dependancies with composer
composer_install() {
	local COMPOSER_VERB
	local COMPOSER_ING
	local RET
	
	printf "\n"
	
	phar_install "/composer.phar" "$PAREDIR/composer.phar" "composer" "composer"
	
	if [ ! -f "$CURRDIR/vendor/autoload.php" ]
	then
		COMPOSER_VERB="install"
		COMPOSER_ING="INSTALLING"
	else
		COMPOSER_VERB="update"
		COMPOSER_ING="UPDATING"
	fi
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] $COMPOSER_ING composer dependancies"
	set +e
	php "$PAREDIR/composer.phar" "$COMPOSER_VERB" --ansi --no-interaction --no-progress --prefer-dist
	RET=$?
	set -e
	
	return $RET
}

# Tries to install composer dependancies multiple times
# @var $1 the number of retries
composer_install_retry() {
	local RET
	local MAXLOOP
	
	composer_install
	RET=$?
	
	MAXLOOP=$(($1))
	if [ -z ${1+x} ]
	then
		MAXLOOP=3
	fi
	
	LOOP=0
	while [[ $LOOP < $MAXLOOP && "$RET" != "0" ]]
	do
		sleep "$((39 * (LOOP + 1)))s"
		LOOP=$((LOOP + 1))
		composer_install
		RET=$?
	done
	
	return $RET
}

# }}} END FUNCTIONS

check_install "awk"
check_install "cat"
check_install "curl"
check_install "grep"
check_install "head"
check_install "jq"
check_install "php"
check_install "sed"
printf "\n"

# @var PHPVERSION string the php version (we get "A.B")
# php -v :: PHP A.B.C (cli) (built: Aug XX YYYY HH:MM:SS) ( NTS )
PHPVERSION=$(php -v | grep PHP | grep cli | awk '{print $2}' | awk 'BEGIN{FS="."} {print $1"."$2}')
PHPVERSIONINT=$(($(php -v | grep PHP | grep cli | awk '{print $2}' | awk 'BEGIN{FS="."} {print $1$2}')))
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUNNING ON PHP : (string) $PHPVERSION / (int) $PHPVERSIONINT"

# @var RUN_IN_CI boolean whether this script runs in CI (gitlab...)
RUN_IN_CI=0
# @var RUN_RESULT_BAN_MIXED integer the exit code of ban of mixed check
RUN_RESULT_BAN_MIXED=0
# @var RUN_RESULT_BAN_SELF integer the exit code of ban of return self
RUN_RESULT_BAN_SELF=0
# @var RUN_RESULT_RETURN_TAB integer the exit code of ban of return X[]
RUN_RESULT_RETURN_TAB=0
# @var RUN_RESULT_PHPCSFIXER integer the exit code of php_cs_fixer
RUN_RESULT_PHPCSFIXER=0;
# @var RUN_RESULT_PHPSTAN integer the exit code of phpstan
RUN_RESULT_PHPSTAN=0
# @var RUN_RESULT_PSALM integer the exit code of psalm
RUN_RESULT_PSALM=0
# @var RUN_RESULT_PHPMD integer the exit code of phpmd
RUN_RESULT_PHPMD=0
# @var RUN_RESULT_PHPUNIT integer the exit code of phpunit
RUN_RESULT_PHPUNIT=0

# argument management loop
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
for arg in "$@"
do
	case $arg in
		--ci*) RUN_IN_CI=1 ;;
		*)                 ;;
	esac
	shift # remove arg from "$@" and reorder $ positions
done


# {{{ BEGIN GENERATE PHPUNIT FILES

printf "\n"

if [ ! -d "$CURRDIR/test" ]
then
	mkdir "$CURRDIR/test"
fi

for FILE in $(find ./src -maxdepth 1 -name '*.php')
do
	
	TESTFILENAME=$(basename "$FILE")
	OBJCLASSNAME="${TESTFILENAME:0:-4}"
	TESTCLASSNAME="${OBJCLASSNAME}Test"
	TESTFILENAME="$TESTCLASSNAME.php"
	TESTFILEPATH=$(realpath "./test/$TESTFILENAME")
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] Checking test file $TESTFILENAME"
	[ -f "$TESTFILEPATH" ] && continue
	
	NAMESPACE=$(jq '.autoload."psr-4" | keys[0]' "$CURRDIR/composer.json" | sed 's/"//g' | sed 's/\\\\/\\/g' | sed 's/\\$//g')
	
	cat > "$TESTFILEPATH" <<EOF
<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use $NAMESPACE\\$OBJCLASSNAME;

/**
 * $TESTCLASSNAME test file.
 * 
 * @author Anastaszor
 * @covers \\$NAMESPACE\\$OBJCLASSNAME
 */
class $TESTCLASSNAME extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var $OBJCLASSNAME
	 */
	protected \$_object;
	
	public function testToString() : void
	{
		\$this->assertEquals(get_class(\$this->_object).'@'.spl_object_hash(\$this->_object), \$this->_object->__toString());
	}
	
	/**
	 * {@inheritdoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		\$this->_object = new $OBJCLASSNAME();
	}
	
	/**
	 * {@inheritdoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		\$this->_object = null;
	}
	
}
EOF
	
done

# }}} END GENERATE PHPUNIT FILES


# {{{ BEGIN FORMAT composer.json
if [ $RUN_IN_CI == 0 ]
then
	
	printf "\n"
	# we dont need to rebuild composer.json hence dont need jq when in CI
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] REORDERING : $CURRDIR/composer.json"
	CURRCOMP=$(cat "$CURRDIR/composer.json")
	# reorder composer.json according to schema
	# https://getcomposer.org/doc/04-schema.md
	# except minimum stability
	# pretty print with tabs
	# then ignore null fields
	# then add space before colon
	echo "$CURRCOMP" | jq --tab '{ 
		name: .name,
		description: .description,
		version: .version,
		type: .type,
		keywords: .keywords,
		homepage: .homepage,
		readme: .readme,
		time: .time,
		license: .license,
		authors: .authors,
		support: .support,
		funding: .funding,
		require: .require,
		"require-dev": ."require-dev",
		conflict: .conflict,
		replace: .replace,
		provide: .provide,
		suggest: .suggest,
		autoload: .autoload,
		"autoload-dev": ."autoload-dev",
		"include-path": ."include-path",
		"target-dir": ."target-dir",
		"prefer-stable": ."prefer-stable",
		repositories: .repositories,
		config: .config,
		scripts: .scripts,
		extra: .extra,
		bin: .bin,
		archive: .archive,
		abandoned: .abandoned,
		"non-feature-branches": ."non-feature-branches"
	} | del(.[] | nulls)' | sed 's/":/" :/g' | head -c -1 > "$CURRDIR/composer.json"
fi
# }}} END FORMAT composer.json


# {{{ BEGIN RUN COMPOSER

composer_install_retry 3

# }}} END RUN COMPOSER


# {{{ BEGIN RUN PHP-CS-FIXER
if [ $PHPVERSIONINT == 81 ]
then
	
	printf "\n"
	phar_install "/vendor/bin/php-cs-fixer" "$PAREDIR/php-cs-fixer.phar" "FriendsOfPHP" "PHP-CS-Fixer"
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN PHP CS FIXER"
	set +e
	if [ $RUN_IN_CI == 0 ]
	then
		php "$PAREDIR/php-cs-fixer.phar" fix -vvv --allow-risky=yes
	else
		php "$PAREDIR/php-cs-fixer.phar" fix -v --allow-risky=yes --dry-run
	fi
	RUN_RESULT_PHPCSFIXER=$?
	set -e
	if [ $RUN_IN_CI == 0 ]
	then
		if [ $RUN_RESULT_PHPCSFIXER != 0 ]
		then
			exit $RUN_RESULT_PHPCSFIXER
		fi
	fi
	
else
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] SKIPPED php-cs-fixer : not php 7.4"
fi
# }}} END RUN PHP-CS-FIXER


# {{{ BEGIN BAN OF MIXED
echo "[$(date '+%Y-%m-%d %H:%M:%S')] CHECK FOR 'MIXED' VALUES IN SOURCE CODE"
set +e
grep -r 'mixed' ./src --include="*.php" > /dev/null
RUN_RESULT_BAN_MIXED=$((1 - $?))
set -e
if [ $RUN_IN_CI == 0 ]
then
	if [ $RUN_RESULT_BAN_MIXED != 0 ]
	then
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] There are still 'mixed' values in the source code, please expand it with null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array>"
		exit 1
	fi
fi
# }}} END BAN OF MIXED


# {{{ BEGIN BAN OF SELF AS RETURN TYPE
# Should be replaced with return type static when php min version is php 8.0
# https://wiki.php.net/rfc/static_return_type
# Until then, return types should return objects of the class it is written in
echo "[$(date '+%Y-%m-%d %H:%M:%S')] CHECK FOR ': SELF' VALUES IN SOURCE CODE"
set +e
grep -r ': self' ./src --include="*.php" > /dev/null
RUN_RESULT_BAN_SELF=$((1 - $?))
set -e
if [ $RUN_IN_CI == 0 ]
then
	if [ $RUN_RESULT_BAN_SELF != 0 ]
	then
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] There are still ': self' return types values in the code, change it to the interface name."
		exit 2
	fi
fi
# }}} END BAN OF self


# {{{ BEGIN BAN OF @return X[] 
echo "[$(date '+%Y-%m-%d %H:%M:%S')] CHECK FOR '@return X[]' VALUES IN SOURCE CODE"
set +e
grep -r '@return *\[\]' ./src --include="*.php" > /dev/null
RUN_RESULT_RETURN_TAB=$((1 - $?))
set -e
if [ $RUN_IN_CI == 0 ]
then
	if [ $RUN_RESULT_RETURN_TAB != 0 ]
	then
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] There are still '@return X[]' return types values in the code, change it to array<integer|string, X>."
		exit 3
	fi
fi
# }}} END BAN OF @return X[]]


printf "\n"

# {{{ BEGIN RUN PHPSTAN

phar_install "/vendor/bin/phpstan" "$PAREDIR/phpstan.phar" "phpstan" "phpstan"

echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN PHPSTAN"
set +e
php "$PAREDIR/phpstan.phar" --version
php "$PAREDIR/phpstan.phar" analyse --configuration="$CURRDIR/phpstan.neon" --error-format=gitlab --memory-limit 2G
RUN_RESULT_PHPSTAN=$?
set -e
if [ $RUN_IN_CI == 0 ]
then
	if [ $RUN_RESULT_PHPSTAN != 0 ]
	then
		exit $RUN_RESULT_PHPSTAN
	fi
fi

# }}} END RUN PHPSTAN

printf "\n"

# {{{ BEGIN RUN PSALM
if (( PHPVERSIONINT >= 74 ))
then
	phar_install "/vendor/bin/psalm" "$PAREDIR/psalm.phar" "vimeo" "psalm"
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] CLEAR PSALM CACHE"
	rm -rf ~/.cache/psalm
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN PSALM"
	set +e
	php "$PAREDIR/psalm.phar" --version
	php "$PAREDIR/psalm.phar" --config="$CURRDIR/psalm.xml" --output-format=console --long-progress --stats --show-info=true --php-version="$PHPVERSION"
	RUN_RESULT_PSALM=$?
	set -e
	if [ $RUN_IN_CI == 0 ]
	then
		if [ $RUN_RESULT_PSALM != 0 ]
		then
			exit $RUN_RESULT_PSALM
		fi
	fi
fi
# }}} END RUN PSALM

printf "\n"

# {{{ BEGIN RUN PHPMD

phar_install "/vendor/bin/phpmd" "$PAREDIR/phpmd.phar" "phpmd" "phpmd"

echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN PHPMD"
set +e
php "$PAREDIR/phpmd.phar" --version
php "$PAREDIR/phpmd.phar" "$CURRDIR/src" ansi "$CURRDIR/phpmd.xml"
RUN_RESULT_PHPMD=$?
set -e
if [ $RUN_IN_CI == 0 ]
then
	if [ $RUN_RESULT_PHPMD != 0 ]
	then
		exit $RUN_RESULT_PHPMD
	fi
fi
# }}} END RUN PHPMD

printf "\n"

# {{{ BEGIN RUN PHPUNIT
rm -rf "$CURRDIR/build/coverage"
phar_install "/vendor/bin/phpunit" "$PAREDIR/phpunit.phar" "sebastianbergmann" "phpunit"

echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN PHPUNIT"
set +e
php "$PAREDIR/phpunit.phar" --configuration "$CURRDIR/phpunit.xml" --coverage-text
RUN_RESULT_PHPUNIT=$?
set -e
if [ $RUN_IN_CI == 0 ]
then
	if [ $RUN_RESULT_PHPUNIT != 0 ]
	then
		exit $RUN_RESULT_PHPUNIT
	fi
fi
rm -f "$CURRDIR/.phpunit.result.cache"
# }}} END RUN PHPUNIT


# https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences
printf "\n"
PASSED="\033[32mPASSED\033[0m"
FAILED="\033[91mFAILED\033[0m"
echo "[$(date '+%Y-%m-%d %H:%M:%S')] ---- CI/CD SCRIPT RESUME ----"
RESULT=$([ $RUN_RESULT_BAN_MIXED == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT BAN MIXED        : $RESULT"
RESULT=$([ $RUN_RESULT_BAN_SELF == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT BAN RETURN SELF  : $RESULT"
RESULT=$([ $RUN_RESULT_RETURN_TAB == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT BAN RETURN TAB[] : $RESULT"
RESULT=$([ $RUN_RESULT_PHPCSFIXER == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT PHPCSFIXER       : $RESULT"
RESULT=$([ $RUN_RESULT_PHPMD == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT PHPMD            : $RESULT"
RESULT=$([ $RUN_RESULT_PHPSTAN == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT PHPSTAN          : $RESULT"
RESULT=$([ $RUN_RESULT_PSALM == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT PSALM            : $RESULT"
RESULT=$([ $RUN_RESULT_PHPUNIT == 0 ] && echo -e "$PASSED" || echo -e "$FAILED")
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUN RESULT PHPUNIT          : $RESULT"
echo "[$(date '+%Y-%m-%d %H:%M:%S')] ---- CI/CD END OF SCRIPT ----"
printf "\n"

EXIT_CODE=$((RUN_RESULT_BAN_MIXED + RUN_RESULT_BAN_SELF + RUN_RESULT_RETURN_TAB + RUN_RESULT_PHPCSFIXER + RUN_RESULT_PHPMD + RUN_RESULT_PHPSTAN + RUN_RESULT_PSALM + RUN_RESULT_PHPUNIT))
echo "[$(date '+%Y-%m-%d %H:%M:%S')] EXIT CODE : $EXIT_CODE"
exit $EXIT_CODE
