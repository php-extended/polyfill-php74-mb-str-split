<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-php74-mb-str-split library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\MbStrSplit;

if(!\function_exists('mb_str_split'))
{
	
	/**
	 * Given a multibyte string, return an array of its characters.
	 * 
	 * This function will return an array of strings, it is a version of
	 * str_split() with support for encodings of variable character size as
	 * well as fixed-size encodings of 1,2 or 4 byte characters. If the length
	 * parameter is specified, the string is broken down into chunks of the
	 * specified length in characters (not bytes). The encoding parameter can
	 * be optionally specified and it is good practice to do so. 
	 * 
	 * @link https://www.php.net/manual/en/function.mb-str-split.php
	 * @param string $string the string to be split
	 * @param integer $length the length of each chunk, in characters
	 * @param ?string $encoding the encoding of the string
	 * @return array<integer, string> the chunks
	 */
	function mb_str_split(string $string , int $length = 1 , ?string $encoding = null) : array
	{
		return MbStrSplit::mbStrSplit($string, $length, $encoding);
	}
	
}
