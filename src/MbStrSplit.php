<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-php74-mb-str-split library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Polyfill;

/**
 * MbStrSplit class file.
 * 
 * This class encapsulates the mb_str_split function.
 * 
 * @author Anastaszor
 */
final class MbStrSplit
{
	
	/**
	 * Given a multibyte string, return an array of its characters.
	 * 
	 * @param string $string
	 * @param integer $splitLength
	 * @param ?string $encoding
	 * @return array<integer, string>
	 */
	public static function mbStrSplit(string $string, int $splitLength = 1, ?string $encoding = null) : array
	{
		$splitLength = \max(1, $splitLength);
		
		if(null === $encoding)
		{
			$encoding = (string) \mb_internal_encoding();
		}
		
		$result = [];
		$length = (int) \mb_strlen($string, $encoding);
		
		for($i = 0; $i < $length; $i += $splitLength)
		{
			$result[] = (string) \mb_substr($string, $i, $splitLength, $encoding);
		}
		
		return $result;
	}
	
}
