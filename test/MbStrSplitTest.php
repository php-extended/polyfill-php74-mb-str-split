<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-php74-mb-str-split library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\MbStrSplit;
use PHPUnit\Framework\TestCase;

/**
 * MbStrSplitTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Polyfill\MbStrSplit
 *
 * @internal
 *
 * @small
 */
class MbStrSplitTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MbStrSplit
	 */
	protected $_object;
	
	public function testSplitEmpty() : void
	{
		$this->assertEquals([], $this->_object->mbStrSplit(''));
	}
	
	public function testSplitOne() : void
	{
		$this->assertEquals(['o', 'n', 'e'], $this->_object->mbStrSplit('one'));
	}
	
	public function testSplitTwo() : void
	{
		$this->assertEquals(['tw', 'o'], $this->_object->mbStrSplit('two', 2));
	}
	
	public function testNativeStr() : void
	{
		$this->assertEquals(['asu', 'dde', 'nph', 'ras', 'e'], \mb_str_split('asuddenphrase', 3));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MbStrSplit();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
